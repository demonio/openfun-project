#ifndef MAP_H
#define MAP_H

#include "openfun.h"
#include <cstdlib>
#include <ctime>
namespace of {
class segment {
	public:
		int id;
		bool walkable;
		//TODO pouzit textury globalni od blocku
		Sprite sprite;
		void set_sprite(int);
		//TODO
};

class matrix {
	public:
		int width;
		int height;
		vector < vector < vector < segment > > > seg;
		bool create(int, int, int, int);
		bool walkable(int, int);
};
}
#endif // MAP_H
