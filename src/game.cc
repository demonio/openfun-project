// game.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
#include "game.h"
#include <time.h>
#include "zdroje.h"

extern RenderWindow window;
vector< buttons > buts;
vector< buttons > menub;
extern vector< character > players;
View view;
View minimapView;
bool display_inv=false;
int px = 640;
int py = 640;
string menus;
string floors = "none";
string objects = "nic";
int pohyb = 0;
of::matrix ground;
sf::Vector2u wsize;

void game_load (bool reload){
	if (reload){
		view.setViewport(FloatRect(0, 0, 1, 1));
		minimapView.setViewport(FloatRect(0.75f, 0, 0.25f, 0.25f));
      		srand(time(NULL));
		int seed = rand();
		cout << "Your seed is " << seed << endl;
		if(! ground.create(3, 64, 64, 1))
				cout << "Unable to create map" << endl;
	}
}

int test = 0;

bool game_update (Event event){
	//TODO
	if (pohyb == 0 and event.mouseButton.button == sf::Mouse::Left){
		if(display_inv){
			if(!players[0].inv.inside(event.mouseButton.x+(px-wsize.x/2), event.mouseButton.y+(py-wsize.y/2),&players[0].equip.pred))
				players[0].equip.inside(event.mouseButton.x+(px-wsize.x/2), event.mouseButton.y+(py-wsize.y/2),&players[0].inv.pred);
		} else {
			players[0].equip.inside(event.mouseButton.x+(px-wsize.x/2), event.mouseButton.y+(py-wsize.y/2),&players[0].inv.pred);
		}
		pohyb = 2;
	}
	if (ground.height == 0){
		return true;
	}
	wsize = window.getSize();
	view.setSize(float(wsize.x), float(wsize.y));
	view.setCenter(px, py);
	minimapView.setCenter(px, py);
	if (pohyb == 0 and event.type == sf::Event::KeyPressed){
		if (event.key.code == Keyboard::S and px > 0){
			if(ground.seg[1][(px - 64)/64][py/64].walkable == true){
				px -= 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::E and py > 0){
			if(ground.seg[1][(px)/64][(py-64)/64].walkable == true){
				py -= 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::F and px < ((ground.width*64)-64)){
			if(ground.seg[1][(px + 64)/64][py/64].walkable == true){
				px += 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::D and py < ((ground.width*64)-64)){
			if(ground.seg[1][(px)/64][(py+64)/64].walkable == true){
				py += 64;
				pohyb = 2;
			}
		}
		if (event.key.code == Keyboard::I){
			display_inv = !display_inv;
			pohyb = 2;
		}
		if (event.key.code == Keyboard::Left and px > 0){
				players[0].get(&ground.seg[1][(px - 64)/64][py/64]);
				pohyb = 2;
		}
		if (event.key.code == Keyboard::Up and py > 0){
				players[0].get(&ground.seg[1][px/64][(py - 64)/64]);
				pohyb = 2;
		}
		if (event.key.code == Keyboard::Right and px < ((ground.width*64)-64)){
				players[0].get(&ground.seg[1][(px + 64)/64][py/64]);
				pohyb = 2;
		}
		if (event.key.code == Keyboard::Down and py < ((ground.width*64)-64)){
				players[0].get(&ground.seg[1][px/64][(py + 64)/64]);
				pohyb = 2;
		}
	} else {
		if (pohyb > 0)
			pohyb--;
	}
	players[0].update(px, py);
	return false;
}

void game_draw (){
	//decrease texture aby se usetrila pamet
	window.setView(view);
	for(unsigned int i=0;i<3;i++){
		for(int j=((px-((int) wsize.x/2))/64)-2;j<((px+((int) wsize.x/2))/64)+2;j++){
			for(int k=((py-((int) wsize.y/2))/64)-2;k<((py+((int) wsize.y/2))/64)+2;k++){
				if(j>-1 and j<ground.height and k>-1 and k<ground.width)
					window.draw(ground.seg[i][j][k].sprite);
			}
		}
	}
	window.draw(players[0].head);;
	window.draw(players[0].nose);
	window.draw(players[0].hair);
	window.draw(players[0].browl);
	window.draw(players[0].browr);
	window.draw(players[0].eyel);
	window.draw(players[0].eyer);
	window.draw(players[0].mounth);
	players[0].equip.draw(&window);
	if(display_inv){
		players[0].inv.draw(&window);
	}
	window.setView(minimapView);
	for(unsigned int i=0;i<3;i++){
		for(int j=((px-((int) wsize.x/2))/64)-2;j<((px+((int) wsize.x/2))/64)+2;j++){
			for(int k=((py-((int) wsize.y/2))/64)-2;k<((py+((int) wsize.y/2))/64)+2;k++){
				if(j>-1 and j<ground.height and k>-1 and k<ground.width)
					window.draw(ground.seg[i][j][k].sprite);
			}
		}
	}
}
