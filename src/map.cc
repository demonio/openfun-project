// map.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
#include "map.h"
#include "zdroje.h"

extern vector< sf::Sprite > images;

void of::segment::set_sprite (int kind){
	this->id = kind;
	this->sprite = images[of::load_image(this->id)];
	this->walkable = true;
}

bool of::matrix::create(int a,int b, int c, int seed){
	srand(seed);

	seg.resize( a, vector < vector < segment > >(b, vector<segment>(c)));
	this->height = b;
	this->width = c;

	for(unsigned int i=0;i<seg.size();i++){
			for(unsigned int j=0;j<seg[i].size();j++){
				for(unsigned int k=0;k<seg[i][j].size();k++){
					if(i == 0)
						seg[i][j][k].id = 1000;
					if(i == 1){
						if(j<2 or k<2 or j>seg[i].size()-4 or k>seg[i][j].size()-4)
							seg[i][j][k].id =2;
						else{
							int number = rand() % 10;
							if(number < 2)
								seg[i][j][k].id = 1;
							else if(number > 8)
								seg[i][j][k].id = 2;
							else
								seg[i][j][k].id = 0;
						}
					}
					if(i == 3)
						seg[i][j][k].id = 0;

				}
			}

		}


		int x = rand() % width;
		int y = rand() % height;
		int radius = rand() % 6;

		for(int i=y-radius;i<y+radius;i++){
			for(int j=x-radius;j<x+radius;j++){
				if(i>0 and j>0 and i<b and j<c){
					if(i<2 or j<2 or j>y+radius-4 or i>y+radius-4)
						seg[1][i][j].id=rand() % 4;
					else
						seg[1][i][j].id=3;

				}
			}
		}

		x = rand() % width;
		y = rand() % height;

		seg[1][x][y].id = 4;

		x = rand() % width;
		y = rand() % height;

		seg[1][x][y].id = 4;

		for(unsigned int i=0;i<seg.size();i++){
					for(unsigned int j=0;j<seg[i].size();j++){
						for(unsigned int k=0;k<seg[i][j].size();k++){
							seg[i][j][k].set_sprite(seg[i][j][k].id);
							seg[i][j][k].sprite.setPosition((float)j*64, (float)k*64);
							if (seg[i][j][k].id > 0 and seg[i][j][k].id < 100)
							seg[i][j][k].walkable = false;
						}
					}
		}

	return true;
}
