/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main.cc
 * Copyright (C) 2015 vaclav <vaclav@linux.com>
 * 
 * sims is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * sims is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//game includes
#include "state.h"
#include "game.h"
#include "zdroje.h"


int w_width = 800;
int w_height = 600;

//background colors
Color Blue = Color(63, 124, 182,255);
Color Green = Color(141, 196, 53,255);
Color Red = Color(255, 10, 10,255);
Color White = Color(255, 255, 255,255);

int state = 1;
bool reconf =  true;
Font cfont;
vector< character > players;
int mx, my;
RenderWindow window(VideoMode(w_width, w_height), "OpenFun");
Text header("Create Character", cfont, 45);

vector< sf::Sprite > images;
sf::Texture texture;

vector< of::item > items;

int main()
{
	if (!texture.loadFromFile("data/images.png")){
			cout << "Cant load image" << endl;
			return EXIT_FAILURE;
	}
	of::generate_images();
	items.resize(5);
	for(unsigned int i=0;i<items.size();i++){
		items[i].load(i);
	}
	if (!cfont.loadFromFile("data/font/kenvector_future.ttf"))
		return EXIT_FAILURE;
    while (window.isOpen())
    {
		mx = Mouse::getPosition(window).x;
		my = Mouse::getPosition(window).y;
		if (state == 1){
			state_load(reconf);
			reconf = false;
		}
		if (state == 2){
			game_load (reconf);
			reconf = false;
		}
        // Process events
        Event event;
        while (window.pollEvent(event))
        {
            // Close window: exit
            if (event.type == sf::Event::Closed)
                window.close();
			if (state == 1)
				reconf = state_update (event);
			if (state == 2)
				reconf = game_update (event);
        }
        // Clear screen
		if(state == 1){
			window.clear(Blue);
			state_draw ();
		}
		if(state == 2){
			window.clear(Green);
			game_draw ();
		}
		// Draw the sprite
        window.display();
    }
    return EXIT_SUCCESS;
}
