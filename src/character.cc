// character.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "character.h"
#include "zdroje.h"
#include "gui.h"

extern vector< of::item > items;
extern Font cfont;
extern sf::Vector2u wsize;

void character::set_hair(string color){
	if(color != "Female" and color != "Male"){
	string tmp = "data/character/" + sex + "/Hair/" + color + ".png";
    if (!hairt.loadFromFile(tmp))
		cout << "cant load hair image " << color << endl;
	hairt.setSmooth(true);
	hair.setTexture(hairt);
	hair.setScale(0.25,0.25);}
}
void character::set_skin(string color){
	if(color.find("tint") != std::string::npos){
	//Head
	string tmp = "data/character/" + sex + "/Skin/" + color + "/" + color + "_head.png";
	if (!headt.loadFromFile(tmp))
		cout << "cant load head image " << color << endl;
	headt.setSmooth(true);
	head.setTexture(headt);
	head.setScale(0.25,0.25);
	//Nose
	string tmp2 = "data/character/" + sex + "/Nose/" + color + "/" + color + "Nose2.png";
	if (!noset.loadFromFile(tmp2))
		cout << "cant load head image " << color << endl;
	noset.setSmooth(true);
	nose.setTexture(noset);
	nose.setScale(0.25,0.25);
/*	//neck
	string tmp2 = "data/character/" + sex + "/Skin/" + color + "/" + color + "_neck.png";
	if (!neckt.loadFromFile(tmp2))
		cout << "cant load neck image " << color << endl;
	neckt.setSmooth(true);
	neck.setTexture(neckt);
	neck.setScale(0.25,0.25);
	//shirt
	string tmp3 = "data/character/" + sex + "/Shirt/test.png";
	if (!shirtt.loadFromFile(tmp3))
		cout << "cant load shirt image " << endl;
	shirtt.setSmooth(true);
	shirt.setTexture(shirtt);
	shirt.setScale(0.25,0.25);
	//arms
	string tmp4 = "data/character/" + sex + "/Skin/" + color + "/" + color + "_arm.png";
	if (!arm.loadFromFile(tmp4))
		cout << "cant load shirt image " << endl;
	arm.setSmooth(true);
	arml.setTexture(arm);
	arml.setScale(0.20,0.20);
	arml.setRotation(35);
	armr.setTexture(arm);
	armr.setScale(-0.20,0.20);
	armr.setRotation(-35);*/
	//hand
	//pants
	//shoes
	
	//TODO
	}
}

void character::set_brow(string color){
	if(color.find("tint") == std::string::npos){
		string tmp = "data/character/" + sex + "/Eyebrows/" + color + ".png";
		if (!brow.loadFromFile(tmp))
			cout << "cant load brow image " << endl;
		brow.setSmooth(true);
		browl.setTexture(brow);
		browl.setScale(0.25,0.25);
		browr.setTexture(brow);
		browr.setScale(-0.25,0.25);
	}
}

void character::set_eye(string color){
	//if(color.find("tint") == std::string::npos){
		string tmp = "data/character/" + sex + "/Eyes/" + color + ".png";
		if (eye.loadFromFile(tmp)){
			eye.setSmooth(true);
			eyel.setTexture(eye);
			eyel.setScale(0.25,0.25);
			eyer.setTexture(eye);
			eyer.setScale(-0.25,0.25);
		}
	//}
}

void character::set_mounth(string color){
	//if(color.find("tint") == std::string::npos){
		string tmp = "data/character/" + sex + "/mouth/mouth_" + color + ".png";
		if (mountht.loadFromFile(tmp)){
			mountht.setSmooth(true);
			mounth.setTexture(mountht);
			mounth.setScale(0.25,0.25);
		}
	//}
}

void character::set_name (){
		if (!namef.loadFromFile("data/font/kenvector_future.ttf"))
			cout << "No font";
		name.setFont(namef);
		name.setCharacterSize(20);
}

void character::get_draw (){
	//TODO
}

void character::update(int px, int py){
	this->head.setPosition(px, py);
	this->nose.setPosition(px+20, py+19);
	this->hair.setPosition(px+2, py-9);
	this->browl.setPosition(px+24, py+11);
	this->browr.setPosition(px+20, py+11);
	this->eyel.setPosition(px+27, py+14);
	this->eyer.setPosition(px+17, py+14);
	this->mounth.setPosition(px+19, py+30);
	this->inv.move(px-152, py-140);
	this->equip.move(px-201, py+((wsize.y/2)-55));
}

bool character::get(of::segment * seg){
	if(seg->id == 0){
			for(unsigned int i = 0;i<this->equip.pred.size();i++){
				if(this->equip.pred[i].pos == 0){
					seg->set_sprite(this->equip.pred[i].id);
					seg->sprite.setColor(sf::Color(255,255,255));
					seg->walkable = false;
					this->equip.pred[i].amount --;
					if(this->equip.pred[i].amount == 0){
						this->equip.pred.erase(this->equip.pred.begin()+i);
					}
					return true;
				}
			}
		}
	if(seg->id == 0)
		return false;
	int red = seg->sprite.getColor().g;
	red -= this->gather/seg->id;
	if(red > 0){
		seg->sprite.setColor(sf::Color(255,red,red));
		return false;
	}
	int id = seg->id;
	of::item_inv item;
	if(this->inv.pred.size() >= 36)
		return false;
	if(id > 0){
		item.id = id;
		for(unsigned int i = 0;i<this->inv.pred.size();i++){
			if(inv.pred[i].id == item.id and inv.pred[i].amount <64){
				inv.pred[i].amount ++;
				seg->set_sprite(0);
				return true;
			}
		}
		item.amount = 1;
		item.text.setFont(cfont);
		item.text.setCharacterSize(20);
		item.text.setStyle(Text::Bold);
		item.sprite.setTexture(items[item.id].inventory);
		item.pos = this->inv.pred.size();
		this->inv.pred.push_back(item);
		seg->set_sprite(0);
		return true;
	}

	return false;
}
