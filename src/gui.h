/*
 * gui.h
 *
 *  Created on: 29.6.2015
 *      Author: venca
 */

#ifndef GUI_H_
#define GUI_H_
#include "openfun.h"
namespace of {

class item_inv {
	public:
		int amount;
		int id;
		int pos;
		Sprite sprite;
		Text text;
		bool selected = false;
};
class item_inv_bg{
	public:
		Sprite sprite;
};

class inventory{
	public:
		Texture bg_texture;
		vector< item_inv > pred;
		vector< item_inv_bg > bg;
		RectangleShape rectangle; //Vector2f(304, 280)
		inventory(void);
		void draw(sf::RenderWindow *);
		void move(int, int);
		bool inside(int, int, vector< item_inv > *);
};

class equip{
	public:
		Texture bg_texture;
		vector< item_inv > pred;
		vector< item_inv_bg > bg;
		RectangleShape rectangle; //Vector2f(304, 280)
		equip(void);
		bool asign(item_inv *);
		bool remove(item_inv *);
		void draw(sf::RenderWindow *);
		void move(int, int);
		bool inside(int, int, vector< item_inv > *);
};

class gui{
	public:
		void move(int, int);
};

}
#endif /* GUI_H_ */
