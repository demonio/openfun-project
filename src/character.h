/*
 * character.h
 *
 * Copyright (C) 2015 - venca
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef CHARACTER_H
#define CHARACTER_H
#include "openfun.h"
#include "map.h"
#include "gui.h"

class character {
	public:
		string sex;
		Texture hairt;
		Sprite hair;
		Texture headt;
		Sprite head;
		Texture brow;
		Sprite browl;
		Sprite browr;
		Texture eye;
		Sprite eyel;
		Sprite eyer;
		Texture noset;
		Sprite nose;
		Texture mountht;
		Sprite mounth;
		Text name;
		Font namef;
/*		Texture neckt;
		Sprite neck;
		Texture shirtt;
		Sprite shirt;
		Texture arm;
		Sprite arml;
		Sprite armr;
		Texture pantst;
		Sprite pants;
		Texture Pants;
		Sprite pantsl;
		Sprite pantsr;*/
		void set_hair(string);
		void set_skin(string);
		void set_brow(string);
		void set_eye(string);
		void set_mounth(string);
		void set_name();
		void get_draw();
		int x, y;
		of::inventory inv;
		of::equip equip;
		void update(int, int);
		bool get(of::segment *);
		bool place(of::segment *);
		//TODO
		//Vytvořit parametry hlad stres atd
		int hp = 10;
		int mp = 10;
		int speed = 10;
		int gather = 25;
		int dmg = 1;
		int def = 1;
};

#endif // CHARACTER_H
