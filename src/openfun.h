#ifndef VAR_H
#define VAR_H
//system includes
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
//#include <dirent.h>
#include <sstream>

#define SSTR( x ) dynamic_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

using namespace std;
using namespace sf;

#endif // VAR_H
