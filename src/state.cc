// state.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
#include "state.h"

//TODO
//mini_state prepsat sem nebude potreba extern
extern RenderWindow window;
int mini_state = 1;
vector< buttons > but;
extern vector< character > players;
extern int w_height;
extern int w_width;
extern int mx;
extern int my;
extern int state;
string str;
extern Color Red;
extern Color White;
extern Text header;
//TODO move here header
//extern Font cfont;

void state_load (bool reconf) {
	if (reconf){
			if (mini_state == 1){
				header.setString("Create character");
				header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
				but.clear();
				but.resize(2);
				players.resize(1);
				but[0].set_values (w_width/2-20, (w_height/2)-20, "Male");
				but[1].set_values (w_width/2-20, (w_height/2)+20, "Female");
			}
			if (mini_state == 2){
					but.clear();
					but.resize(7);
					header.setString("Choose Hair color");
					header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
					but[0].set_values (w_width/2-200, (w_height/2)-70, "Black");
					but[1].set_values (w_width/2-200, (w_height/2)-50, "Blonde");
					but[2].set_values (w_width/2-200, (w_height/2)-30, "Brown");
					but[3].set_values (w_width/2-200, (w_height/2)-10, "Grey");
					but[4].set_values (w_width/2-200, (w_height/2)+10, "Red");
					but[5].set_values (w_width/2-200, (w_height/2)+30, "Tan");
					but[6].set_values (w_width/2-200, (w_height/2)+50, "White");
			}
			if (mini_state == 3){
					but.clear();
					but.resize(8);
					header.setString("Choose Skin color");
					header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
					but[0].set_values (w_width/2-200, (w_height/2)-70, "tint1");
					but[1].set_values (w_width/2-200, (w_height/2)-50, "tint2");
					but[2].set_values (w_width/2-200, (w_height/2)-30, "tint3");
					but[3].set_values (w_width/2-200, (w_height/2)-10, "tint4");
					but[4].set_values (w_width/2-200, (w_height/2)+10, "tint5");
					but[5].set_values (w_width/2-200, (w_height/2)+30, "tint6");
					but[6].set_values (w_width/2-200, (w_height/2)+50, "tint7");
					but[7].set_values (w_width/2-200, (w_height/2)+70, "tint8");
			}
			if (mini_state == 4){
					but.clear();
					but.resize(7);
					header.setString("Choose Eyebrows color");
					header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
					but[0].set_values (w_width/2-200, (w_height/2)-70, "Black");
					but[1].set_values (w_width/2-200, (w_height/2)-50, "Blonde");
					but[2].set_values (w_width/2-200, (w_height/2)-30, "Brown");
					but[3].set_values (w_width/2-200, (w_height/2)-10, "Grey");
					but[4].set_values (w_width/2-200, (w_height/2)+10, "Red");
					but[5].set_values (w_width/2-200, (w_height/2)+30, "Tan");
					but[6].set_values (w_width/2-200, (w_height/2)+50, "White");
			}
			if (mini_state == 5){
					but.clear();
					but.resize(5);
					header.setString("Choose Eye color");
					header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
					but[0].set_values (w_width/2-200, (w_height/2)-70, "Black");
					but[1].set_values (w_width/2-200, (w_height/2)-50, "Blue");
					but[2].set_values (w_width/2-200, (w_height/2)-30, "Brown");
					but[3].set_values (w_width/2-200, (w_height/2)-10, "Green");
					but[4].set_values (w_width/2-200, (w_height/2)+10, "Pine");
			}
			if (mini_state == 6){
					but.clear();
					but.resize(7);
					header.setString("Choose Mounth");
					header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
					but[0].set_values (w_width/2-200, (w_height/2)-70, "glad");
					but[1].set_values (w_width/2-200, (w_height/2)-50, "happy");
					but[2].set_values (w_width/2-200, (w_height/2)-30, "oh");
					but[3].set_values (w_width/2-200, (w_height/2)-10, "sad");
					but[4].set_values (w_width/2-200, (w_height/2)+10, "straight");
					but[5].set_values (w_width/2-200, (w_height/2)+30, "teethLower");
					but[6].set_values (w_width/2-200, (w_height/2)+50, "teethUpper");
			}
			if (mini_state == 7){
				but.clear();
				but.resize(1);
				header.setString("Enter Name");
				header.setPosition((w_width/2)-(header.getLocalBounds().width/2),10);
				but[0].set_values (w_width/2, (w_height/2), "Submit");
			}
	}
}

bool state_update (Event event) {
	//cout << mini_state << endl;
	if (event.type == Event::MouseButtonPressed){
		for (unsigned i=0; i<but.size(); i++)
			if (but[i].check(mx,my)){
				if(mini_state == 1){
						players[0].sex = but[i].show.getString();
						mini_state = 2;
						return true;
				}
				if(players[0].hair.getTexture()){
					if(mini_state == 2){
						mini_state=3;
						return true;
					}
				}
				if(players[0].head.getTexture()){
					if(mini_state == 3){
						mini_state=4;
						return true;
					}
				}
				if(players[0].browl.getTexture()){
					if(mini_state == 4){
						mini_state=5;
						return true;
					}
				}
				if(players[0].eyel.getTexture()){
					if(mini_state == 5){
						mini_state=6;
						return true;
					}
				}
				if(players[0].mounth.getTexture()){
					if(mini_state == 6){
						mini_state=7;
						players[0].set_name();
						return true;
					}
				}
				if(mini_state == 7 and but[i].show.getString() == "Submit"){
					state = 2;
					return true;
				}
			}
	}
	if(mini_state == 7){
		if (event.type == sf::Event::TextEntered){
			if (event.text.unicode < 128){
				str += static_cast<char>(event.text.unicode);
				cout << string(players[0].name.getString()) << endl;
				players[0].name.setString(str);
			}
		}
		if (event.key.code == sf::Keyboard::BackSpace){
			str = str.substr(0, str.size()-1);
		}
		if (event.key.code == sf::Keyboard::Return){
			state = 2;
		}
	}
	for (unsigned i=0; i<but.size(); i++)
		if (but[i].check(mx,my)){
			but[i].show.setColor(Color(Red));
			if(mini_state == 2){
				players[0].set_hair(but[i].show.getString());
			}
			if(mini_state == 3){
				players[0].set_skin(but[i].show.getString());
			}
			if(mini_state == 4){
				players[0].set_brow(but[i].show.getString());
			}
			if(mini_state == 5){
				players[0].set_eye(but[i].show.getString());
			}
			if(mini_state == 6){
				players[0].set_mounth(but[i].show.getString());
			}
		}else{
			but[i].show.setColor(Color(White));
		}
	return false;
}

void state_draw () {
	window.draw(header);
	for (unsigned i=0; i<but.size(); i++)
		window.draw(but[i].show);
	if(players[0].head.getTexture()){
		players[0].head.setPosition(w_width-202, (w_height/2)-141);
		window.draw(players[0].head);
		players[0].nose.setPosition(w_width-182, (w_height/2)-122);
		window.draw(players[0].nose);
/*			players[0].neck.setPosition(w_width-192, (w_height/2)-100);
		window.draw(players[0].neck);
		//--
		players[0].arml.setPosition(w_width-164, (w_height/2)-95);
		window.draw(players[0].arml);
		players[0].armr.setPosition(w_width-198, (w_height/2)-95);
		window.draw(players[0].armr);*/
	}
/*		if(players[0].shirt.getTexture()){
		players[0].shirt.setPosition(w_width-200, (w_height/2)-100);
		window.draw(players[0].shirt);
	}*/
	if(players[0].hair.getTexture()){
		players[0].hair.setPosition(w_width-200, (w_height/2)-150);
		window.draw(players[0].hair);
	}
	if(players[0].browl.getTexture()){
		players[0].browl.setPosition(w_width-178, (w_height/2)-130);
		window.draw(players[0].browl);
		players[0].browr.setPosition(w_width-182, (w_height/2)-130);
		window.draw(players[0].browr);
	}
	if(players[0].eyel.getTexture()){
		players[0].eyel.setPosition(w_width-175, (w_height/2)-127);
		window.draw(players[0].eyel);
		players[0].eyer.setPosition(w_width-185, (w_height/2)-127);
		window.draw(players[0].eyer);
	}
	if(players[0].mounth.getTexture()){
		players[0].mounth.setPosition(w_width-183, (w_height/2)-110);
		window.draw(players[0].mounth);
	}
	if(players[0].name.getString() != ""){
		players[0].name.setPosition(w_width/2-100, (w_height/2)-100);
		window.draw(players[0].name);
	}
}
