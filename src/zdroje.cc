#include "zdroje.h"

extern vector< sf::Sprite > images;
extern sf::Texture texture;

bool of::item::load(int a){
	string tmp = "data/ground/";
	tmp += SSTR(a);
	tmp += ".png";
	if (!this->ground.loadFromFile(tmp)){
		cout << "cant load ground image " << a << endl;
		return false;
	}

	tmp = "data/items/";
	tmp += SSTR(a);
	tmp += ".png";
	if (!this->inventory.loadFromFile(tmp)){
		cout << "cant load item image " << a << endl;
		return false;
	}

	return true;
}

void of::generate_images(){
	for(unsigned int i=0;i<526;i+=17){
			for(int j=0;j<968;j+=17){
				sf::Sprite tmp;
				tmp.setTexture(texture);
				tmp.setTextureRect(sf::IntRect(j, i, 16, 16));
				tmp.setScale(sf::Vector2f(4, 4));
				images.push_back(tmp);
			}
	}
	cout << images.size() << endl;
}

int of::load_image(int id){
	//TODO
	int load;
	if(id == 0)
		load = 285;
	if(id == 1)
		load = 1251;
	if(id == 2)
		load = 526;
	if(id == 3)
		load = 0;
	if(id == 4)
		load = 1251;
	if(id == 1000)
		load = 5;
	return load;
}
