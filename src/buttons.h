#ifndef BUTTONS_H
#define BUTTONS_H

#include "openfun.h"

class buttons {
	public:
		int x,y;
		void set_values(int, int, string);
		bool check(int, int);
		void draw();
		Text show;
		Font font;
		Sprite bg;
		Texture unchecked;
		Texture checked;
};

#endif // BUTTONS_H
