// buttons.cc
//
// Copyright (C) 2015 - venca
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "buttons.h"

void buttons::set_values(int x_pos, int y_pos, string display){
	string tmp = display;
	x = x_pos;
	y = y_pos;
	this->show.setPosition(float(x),float(y));
	this->show.setString(display);
	if (!this->font.loadFromFile("data/font/kenvector_future.ttf"))
		cout << "No font";
	if (!this->unchecked.loadFromFile("data/buttons/button.png"))
        	//cout << "No image";
	if (!this->checked.loadFromFile("data/buttons/button-check.png"))
        	//cout << "No image";
	this->bg.setTexture(this->unchecked);
	this->show.setFont(font);
	this->show.setCharacterSize(20);
}

bool buttons::check(int cx, int cy){
	if (cx > x and cy > y and cx < x + show.getLocalBounds().width and cy < y + show.getLocalBounds().height){
		this->bg.setTexture(this->checked);
		return true;
	}
	this->bg.setTexture(this->unchecked);
	return false;
}

void buttons::draw(){
	
}
