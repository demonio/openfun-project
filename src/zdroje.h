#ifndef ZDROJE_H
#define ZDROJE_H

#include "openfun.h"

namespace of {
class item {
	public:
		int id;
		Texture ground;
		Texture inventory;
		bool load(int);
};
void generate_images(void);
int load_image(int);
}
#endif // ZDROJE_H
