/*
 * gui.cc
 *
 *  Created on: 29.6.2015
 *      Author: venca
 */
#include "gui.h"

void of::inventory::create(){
	rectangle.setSize(Vector2f(304, 280));
	rectangle.setFillColor(Color(139,69,19));
	rectangle.setPosition(0,0);
	if (!this->bg_texture.loadFromFile("data/gui/bg.png")){
			cout << "cant load inventory image "<< endl;
	}
	int a=0;
	this->bg.resize(36);
	for(unsigned int i = 0;i<6;i++){
		for(unsigned int j = 0;j<6;j++){
			bg[a].sprite.setTexture(bg_texture);
			bg[a].sprite.setPosition((i*49)+5, (j*45)+5);
			a++;
		}
	}
}

void of::inventory::draw(sf::RenderWindow * window){
	window->draw(this->rectangle);
	for(unsigned int i=0;i<this->bg.size();i++){
		window->draw(this->bg[i].sprite);
	}
	for(unsigned int i=0;i<this->pred.size();i++){
		window->draw(this->pred[i].sprite);
		window->draw(this->pred[i].text);
	}
}

void of::inventory::move(int x, int y){
	this->rectangle.setPosition(x, y);
	int a=0;
	for(unsigned int i = 0;i<6;i++){
		for(unsigned int j = 0;j<6;j++){
			this->bg[a].sprite.setPosition(x+(i*49)+5, y+(j*45)+5);
			a++;
		}
	}
	for(unsigned int i = 0;i<this->pred.size();i++){
		this->pred[i].sprite.setPosition(this->bg[this->pred[i].pos].sprite.getPosition().x + 6, this->bg[this->pred[i].pos].sprite.getPosition().y + 4);
		this->pred[i].text.setString(SSTR(this->pred[i].amount));
		this->pred[i].text.setPosition(this->bg[this->pred[i].pos].sprite.getPosition().x +6, this->bg[this->pred[i].pos].sprite.getPosition().y +4);
	}
}

bool of::inventory::inside(int x, int y, vector< item_inv > * pred_tmp){
	int x_ref = this->rectangle.getPosition().x;
	int y_ref = this->rectangle.getPosition().y;
	int x_size = this->rectangle.getSize().x;
	int y_size = this->rectangle.getSize().y;
	if(x>x_ref and x<x_ref+x_size and y>y_ref and y<y_ref+y_size){
		bool set = false;
		for(unsigned int i = 0;i<this->pred.size();i++){
			int x2_ref = this->pred[i].sprite.getPosition().x;
			int y2_ref = this->pred[i].sprite.getPosition().y;
			if(x>x2_ref and x<x2_ref+32 and y>y2_ref and y<y2_ref+32){
				this->pred[i].selected = true;
				set = true;
			} else {
				this->pred[i].selected = false;
			}
		}
		if (set)
			return true;
		of::item_inv item;
		int number = -1;
		for(unsigned int i = 0;i<pred_tmp->size();i++){
			if(pred_tmp[i].data()->selected){
				item = pred_tmp->at(i);
				number = i;

			}
		}
		if(number == -1)
			return false;
		for(unsigned int i = 0;i<this->bg.size();i++){
			int x2_ref = this->bg[i].sprite.getPosition().x;
			int y2_ref = this->bg[i].sprite.getPosition().y;
			if(x>x2_ref and x<x2_ref+49 and y>y2_ref and y<y2_ref+45){
				if(item.id){
					item.pos = i;
					this->pred.push_back(item);
					pred_tmp->erase(pred_tmp->begin()+number);
					return true;
				}
			}
		}
	}
	return false;
}



void of::equip::create(){
	rectangle.setSize(Vector2f(402, 55));
	rectangle.setFillColor(Color(139,69,19));
	rectangle.setPosition(0,0);
	if (!this->bg_texture.loadFromFile("data/gui/bg.png")){
			cout << "cant load inventory image "<< endl;
	}
	this->bg.resize(8);
	for(unsigned int i = 0;i<8;i++){
			bg[i].sprite.setTexture(bg_texture);
			bg[i].sprite.setPosition((i*49)+5, 5);
	}
}

void of::equip::draw(sf::RenderWindow * window){
	window->draw(this->rectangle);
	for(unsigned int i=0;i<this->bg.size();i++){
		window->draw(this->bg[i].sprite);
	}
	for(unsigned int i=0;i<this->pred.size();i++){
		window->draw(this->pred[i].sprite);
		window->draw(this->pred[i].text);
	}
}

void of::equip::move(int x, int y){
	this->rectangle.setPosition(x, y);
	for(unsigned int i = 0;i<8;i++){
			this->bg[i].sprite.setPosition(x+(i*49)+5, y+5);
	}
	for(unsigned int i = 0;i<this->pred.size();i++){
		this->pred[i].sprite.setPosition(this->bg[this->pred[i].pos].sprite.getPosition().x + 6, this->bg[this->pred[i].pos].sprite.getPosition().y + 4);
		this->pred[i].text.setString(SSTR(this->pred[i].amount));
		this->pred[i].text.setPosition(this->bg[this->pred[i].pos].sprite.getPosition().x +6, this->bg[this->pred[i].pos].sprite.getPosition().y +4);
	}
}

bool of::equip::inside(int x, int y, vector< item_inv > * pred_tmp){
	int x_ref = this->rectangle.getPosition().x;
	int y_ref = this->rectangle.getPosition().y;
	int x_size = this->rectangle.getSize().x;
	int y_size = this->rectangle.getSize().y;
	if(x>x_ref and x<x_ref+x_size and y>y_ref and y<y_ref+y_size){
		bool set = false;
		for(unsigned int i = 0;i<this->pred.size();i++){
			int x2_ref = this->pred[i].sprite.getPosition().x;
			int y2_ref = this->pred[i].sprite.getPosition().y;
			if(x>x2_ref and x<x2_ref+32 and y>y2_ref and y<y2_ref+32){
				this->pred[i].selected = true;
				set = true;
			} else {
				this->pred[i].selected = false;
			}
		}
		if (set)
			return true;
		of::item_inv item;
		int number = -1;
		for(unsigned int i = 0;i<pred_tmp->size();i++){
			if(pred_tmp[i].data()->selected){
				item = pred_tmp->at(i);
				number = i;

			}
		}
		if(number == -1)
			return false;
		for(unsigned int i = 0;i<this->bg.size();i++){
			int x2_ref = this->bg[i].sprite.getPosition().x;
			int y2_ref = this->bg[i].sprite.getPosition().y;
			if(x>x2_ref and x<x2_ref+49 and y>y2_ref and y<y2_ref+45){
				if(item.id){
					item.pos = i;
					this->pred.push_back(item);
					pred_tmp->erase(pred_tmp->begin()+number);
					return true;
				}
			}
		}
	}
	return false;
}
