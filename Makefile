all:
	mkdir bin
	g++-4.8 -c src/*.cc -std=c++11
	g++-4.8 *.o -o bin/game.linux_64 -lsfml-graphics -lsfml-window -lsfml-system
clear:
	rm -Rf bin
	rm *.o
linux_64:
	mkdir bin
	g++-4.8 -c src/*.cc -std=c++11
	g++-4.8 *.o -o bin/game.linux_64 -lsfml-graphics -lsfml-window -lsfml-system
	rm *.o
	rm -Rf src/
	touch start
	echo "#!/bin/bash" > start
	echo "./bin/game.linux_64" >> start
	chmod +x start
	rm generate-doc.sh
	rm -Rf .git
	rm Makefile
